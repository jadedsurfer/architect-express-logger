"use strict";
var middleware = require("morgan");

module.exports = function(options, imports, register) {
  var debug = imports.debug("express:middleware:logger");
  debug("start");

  if (process.env.NODE_ENV === 'development') {
    debug(".useSetup in development");
    options.format = options.format || 'dev';
    imports.express.useSetup(middleware(options.format, options.options));
  } else {
    debug(".useSetup");
    imports.express.useSetup(middleware(options.format, options.options));
  }

  debug("register nothing");
  register(null, {});
};
